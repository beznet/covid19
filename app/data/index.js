import parse from 'csv-parse/lib/sync'
import axios from 'axios'
import memoize from 'memoizee'

const getData = () => axios.get('https://static.usafacts.org/public/data/covid-19/covid_confirmed_usafacts.csv')
  .then(({ data }) => parseCSV(data))
  .then(csv => transformData(csv))

const memoizedGetData = memoize(getData, { promise: true })

const getDataByCounties = ids => memoizedGetData()
  .then(data => data.filter(d => ids.includes(d.id)))

const memoizedGetDataByCounties = memoize(getDataByCounties, { promise: true })

export const getTotalCasesByCounties = (key, { ids }) => {
  return memoizedGetDataByCounties(ids)
    .then(data => data
      .map(({ label, points }) => ({ id: label, data: points }))
    )
}

const calculateNewCases = points => {
  return points.map((point, index) => {
    if (index === 0) {
      return point
    }
    return {
      x: point.x,
      y: point.y - points[index - 1].y
    }
  })
}

export const getNewCasesByCounties = (key, { ids }) => {
  return memoizedGetDataByCounties(ids)
    .then(data => data
      .map(({ label, points }) => ({
        id: label,
        data: calculateNewCases(points)
      }))
    )
}

export const getCounties = (key) => {
  return memoizedGetData()
    .then(data => data.map(({ id, label, county, state, stateFIPS, countyFIPS }) => ({
      id,
      label,
      county,
      state,
      stateFIPS,
      countyFIPS
    })
    ))
}

const parseCSV = data => {
  const [columns, ...csvData] = parse(data)
  return {
    columns: transformColumns(columns),
    data: csvData
  }
}

const csvRowToObj = (columns, r) => {
  return r.reduce((memo, value, index) => {
    const column = columns[index]
    if (column instanceof Date) {
      if (parseInt(value) !== 0) {
        memo.points.push({
          x: column,
          y: value
        })
      }
    } else {
      memo[column] = value
    }
    return memo
  }, { points: [] })
}

const transformData = ({ columns, data }) => {
  return data.map(d => {
    const countyObj = csvRowToObj(columns, d)
    return {
      ...countyObj,
      id: `${countyObj.stateFIPS}-${countyObj.countyFIPS}`,
      label: `${countyObj.county} - ${countyObj.state}`
    }
  })
}

const DATE_REGEX = /^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2,4})$/
const transformDateColumn = column => {
  const [m, d, y] = column.split('/')
  const year = y.length === 2 ? parseInt(y) + 2000 : parseInt(y)

  return new Date(year, parseInt(m - 1), parseInt(d))
}
const transformColumns = columns =>
  columns.map(c => {
    if (DATE_REGEX.test(c)) {
      return transformDateColumn(c)
    } else if (c === 'State') {
      return 'state'
    } else if (c === 'County Name') {
      return 'county'
    } else {
      return c
    }
  })
