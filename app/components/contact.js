function Contact(){
  return(
    <div className="mt-5">
      <h3 className="text-purple-600 text-xl font-bold mb-2">Contact</h3>
      Made by <a className="text-blue-600" href="https://twitter.com/gongonzabar">@gongonzabar</a>
      <br />

      Got requests or questions? &nbsp;
      <a className="text-blue-600" href="https://gitlab.com/infinitesecond/covid19/-/issues">File an Issue on Gitlab</a>
    </div>
  )
}
export default Contact
