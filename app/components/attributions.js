function Attributions () {
  return (
    <div className="mt-20">
      <h3 className="text-purple-600 text-xl font-bold mb-2">Data Source</h3>
      Made with data from&nbsp;
      <a
        href="https://usafacts.org/visualizations/coronavirus-covid-19-spread-map/"
        rel="noopener nofollow noreferrer"
        className="underline font-semibold text-purple-600"
      >
        USAFacts.
      </a>

      <br />
      Thank you USAFacts!
    </div>
  )
}

export default Attributions
