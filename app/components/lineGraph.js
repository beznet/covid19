import { memo } from 'react'
import dayjs from 'dayjs'
import { ResponsiveLine } from '@nivo/line'

const numericComparison = (a, b) => {
  if (a < b) { return 1 } else if (a > b) { return -1 } else { return 0 }
}
const customTooltip = ({ slice }) => {
  const sortedPoints = slice.points.sort((a, b) => numericComparison(a.data.y, b.data.y))
  return (
    <div className="w-56 px-2 pt-4 pb-6 bg-white border border-solid border-black border-rounded shadow font-bold">
      <div className="text-center mb-2">{slice.points[0].data.xFormatted}</div>
      {sortedPoints.map(p => (
        <div key={p.id} className="text-sm flex flex-row justify-between">
          <div style={{ color: p.serieColor }}> {p.serieId} </div>
          <div> {p.data.yFormatted} </div>
        </div>
      ))}
    </div>
  )
}

const graphOptions = {
  xScale: {
    type: 'time',
    precision: 'day'
  },
  yScale: {
    type: 'linear'
  },
  legends: [
    {
      anchor: 'bottom-left',
      direction: 'row',
      justify: false,
      translateY: 65,
      translateX: -20,
      itemWidth: 160,
      itemHeight: 20,
      itemsSpacing: 15,
      symbolSize: 20,
      symbolShape: 'circle',
      itemDirection: 'left-to-right'
    }
  ],
  xFormat: d => dayjs(d).format('YYYY-MM-DD'),
  margin: {
    bottom: 200,
    top: 20,
    left: 50,
    right: 50
  },
  axisBottom: {
    format: '%b %d',
    tickValues: 'every 7 days'
  },
  colors: { scheme: 'dark2' },
  sliceTooltip: customTooltip,
  enableSlices: 'x'
}

function LineGraph ({ data, customProperties }) {
  const options = Object.assign({}, graphOptions, customProperties)

  return (
    <ResponsiveLine
      data={data}
      {...options}
    />
  )
}

const MemoizedLineGraph = memo(LineGraph)

export default MemoizedLineGraph
