import { memo } from 'react'
import Select from 'react-windowed-select'

const getLabel = o => o.label
const getValue = o => o.id

function CountySelector ({ counties, onSelect, defaultValue }) {
  return (
    <Select
      defaultValue={defaultValue}
      options={counties}
      getOptionLabel={getLabel}
      getOptionValue={getValue}
      isMulti={true}
      onChange={onSelect}
    />
  )
}

const MemoizedCountySelector = memo(CountySelector)

export default MemoizedCountySelector
