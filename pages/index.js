import { useState } from 'react'
import { useQuery } from 'react-query'

import { getCounties } from '../app/data/index'

import CountiesSelect from '../app/components/countySelector'
import Attributions from '../app/components/attributions'
import Contact from '../app/components/contact'
import TotalCases from '../app/components/totalCases'
import NewCases from '../app/components/newCases'


const INITIAL_COUNTY_ID = '22-22103'
const GRAPH_TYPES = {
  TOTAL_CASES: 'TOTAL_CASES',
  NEW_CASES: 'NEW_CASES',
  INITIAL: 'TOTAL_CASES'
}

const Home = () => {
  const [selectedCounties, setSelectedCounties] = useState([INITIAL_COUNTY_ID])
  const [graphType, setGraphType] = useState(GRAPH_TYPES.INITIAL) // eslint-disable-line 

  const onSelectHandler = (value) => {
    if (value) {
      setSelectedCounties(value.map(v => v.id))
    } else {
      setSelectedCounties([])
    }
  }

  const { data: counties, status: countiesStatus } = useQuery('counties', getCounties)

  return (
    <div className="container p-4 mx-auto">
      <h1 className='text-purple-600 text-3xl font-bold pb-10'>G.K.&apos;s COVID-19 Data Visualizer </h1>
      {countiesStatus === 'success' &&
        <CountiesSelect counties={counties} onSelect={onSelectHandler} />
      }

      <TotalCases selectedCounties={selectedCounties} />
      <NewCases selectedCounties={selectedCounties} />
      <Attributions />
      <Contact />
    </div>
  )
}

export default Home
